import subprocess, sys
import minecraft_launcher_lib as mc
uname = sys.argv[1]
upass = sys.argv[2]
mcdir = "/workspace/1.8.9-mcp/.minecraft"
mcdata = mc.account.login_user(uname, upass)
mc.install.install_minecraft_version("1.8.9", mcdir)
cmd = mc.command.get_minecraft_command("1.8.9", mcdir, {"username": mcdata['selectedProfile']['name'], "uuid": mcdata['selectedProfile']['id'], "token": mcdata['accessToken']})
subprocess.call(cmd)
mc.account.logout_user(uname, upass)